// groupBy
/*
def groupByFirstChar(strs:List[String])={
  strs.groupBy(str=>str.head)

}



var strs:List[String]=List("Johm","Jack","Tom","Hellen")

groupByFirstChar(strs)



def groupByIsOdd(nums:List[Int])={
  nums.groupBy(num=>num %2==1)
}

var nums:List[Int]=List(1,2,3,4,5)
groupByIsOdd(nums)

*/

/*

// sortBy
def sortByKey(kvs:List[(Int, String)])={
  kvs.sortBy(kv=>kv._1)
}


var k:List[(Int,String)]=List(3->"a",2->"b",4->"e")

sortByKey(k)


*/

/*
var l:List[Int]= List(1, 2, 3, 4,52)

def s(nums:List[Int])={
  //nums.sortBy(k => scala.util.Random.nextInt())

}
*/

/*
var nums:List[Int]=List(40, 30, 20, 10)


def sum(nums:List[Int])= {
  nums.reduce((acc,x)=>acc+x)
}

sum(nums)


var n:List[Int]=List(1,2,3,4,5)

def f(n:List[Int])={
      n.reduce((acc,x)=> acc*x)

}

f(n)
*/

/*
var strs:List[String]=List(Hello, I, am, Mark)

def mkString(strs:List[String])= {
  strs.reduce((acc,str)=>acc+" "+str)
}
*/




/*
def sum(nums:List[Int])= {
  nums.reduce((acc,x)=>acc+x)

}
def product(nums:List[Int])= {
  nums.reduce((acc,x)=>acc/x)

}
def mkString(strs:List[String])= {
  strs.reduce((acc,str)=>acc+" "+str)
}

*/


def groupByFirstChar(strs:List[String]):List[String={
  strs.groupBy(str=>str.head)

}


groupByFirstChar(List("Apple", "Banana", "Apple", "Cherry", "Cherry", "Apple")).
